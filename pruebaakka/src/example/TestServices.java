package example;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import example.actores.Espadachin;
import example.actores.Herrero;
import example.actores.Minero;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService()
public class TestServices {

    public static ActorRef espadachin1;
    public static ActorRef espadachin2;
    public static ActorRef herrero;
    public static ActorRef minero;
    public static ActorSystem actorSystem;

    private static final long TIEMPO_ESPERA = 1000;

    @WebMethod()
    public int suma(@WebParam(name = "a") int a, @WebParam(name = "b") int b){
        return a+b;
    }

    @WebMethod()
    public int resta(@WebParam(name = "a") int a, @WebParam(name = "b") int b){
        return a-b;
    }

    @WebMethod()
    public String mensaje() throws InterruptedException {
        System.out.println("Se esta probando");
        actorSystem = ActorSystem.create("ActorSystem");
        espadachin1 = actorSystem.actorOf(Props.create(Espadachin.class), "espadachin1");
        espadachin2 = actorSystem.actorOf(Props.create(Espadachin.class), "espadachin2");
        herrero = actorSystem.actorOf(Props.create(Herrero.class), "herrero");
        minero = actorSystem.actorOf(Props.create(Minero.class), "minero");

        espadachin1.tell(Espadachin.Mensaje.ESPADA_ROTA, ActorRef.noSender());
        Thread.sleep(TIEMPO_ESPERA);
        espadachin2.tell(Espadachin.Mensaje.ESPADA_ROTA, ActorRef.noSender());
        Thread.sleep(3000);
        return "Pueba";
    }
}
