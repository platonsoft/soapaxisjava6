package seguridad;

import org.apache.ws.security.WSPasswordCallback;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

public class UTPasswordCallback implements CallbackHandler {

    private Map<String, String> passwords = new HashMap<String, String>();

    public UTPasswordCallback() {
        System.out.println("Entramos 1");
        passwords.put("cxf", "cxf");
    }


    @Override
    public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
        System.out.println("Entramos 2");
        for (Callback callback : callbacks) {
            if (callback instanceof WSPasswordCallback){
                WSPasswordCallback pc = (WSPasswordCallback) callback;
                String pass = passwords.get(pc.getPassword());
                if (pass != null) {
                    pc.setPassword(pass);
                    return;
                }else {
                    throw new UnsupportedCallbackException(pc, "Acceso denegado");
                }
            }
        }
    }


}
